
var http = require('http');
var https = require('https');
var url = require('url');


var get = function get(url,data,fn){
    http.get(url,(resflight)=>{
        var html = ""
        resflight.on("data",(data)=>{
            html+=data
        })

        resflight.on("end",()=>{
           // console.log(html);
            fn!=undefined && fn(html);

        })
    }).on("error",(e)=>{
        console.log(`获取数据失败: ${e.message}`)
    }).end()
}

var post = function post(url,data,fn){
    data=data||{};
    var content=require('querystring').stringify(data);
    var parse_u=require('url').parse(url,true);
    var isHttp=parse_u.protocol=='http:';
    var options={
        host:parse_u.hostname,
        port:parse_u.port||(isHttp?80:443),
        path:parse_u.path,
        method:'POST',
        headers:{
            'Content-Type':'application/x-www-form-urlencoded',
            'Content-Length':content.length
        }
    };
    var req = require(isHttp?'http':'https').request(options,function(res){
        var _data='';
        res.on('data', function(chunk){
            _data += chunk;
        });
        res.on('end', function(){
            fn!=undefined && fn(_data);
        });
    });
    req.write(content);
    req.end();
}

module.exports = {
    post: post,
    get:get
}