var mysql = require('mysql');
var httpserver = require('http');
var querystring = require('querystring');
var url = require('url');
var share = require('./share.js')
var connection = share.connection;
var pool = share.pool;

function order(param, fn) {
    var result = '';
    //connection.connect();
    var sql = 'SELECT *  from trip where phonenumber=? and status<>9 order by uuid desc';

    var deparam = decodeURIComponent(querystring.stringify(param));
    var pparam = querystring.parse(deparam);
    var sql_Params = [param.phonenumber];
    console.log('Sql_Params');
    console.log(sql_Params);


    connection.query(sql, sql_Params,
        function (error, rows, fields) {

            if (error) {

                console.log(error);

                return;

            }
            result = JSON.stringify(rows);

            console.log('订单查询结果：');
            console.log(result);
            fn(result);

        });
    return result;
    //connection.end();
}

function updateorder(param, fn) {
    var result = '';
    //connection.connect();
    var sql = 'update trip set useflag=?,payflag=?,refundflag=?,evaflag=?,tradeid=?,status=?,evalevel=?,content=? where uuid=?';
    var deparam = decodeURIComponent(querystring.stringify(param));
    var pparam = querystring.parse(deparam);
    console.log('执行sql:' + sql);
    console.log(pparam);

    var Sql_Params = [
        pparam.useflag
        , pparam.payflag
        , pparam.refundflag
        , pparam.evaflag
        , pparam.tradeid
        , pparam.status
        , pparam.evalevel
        , pparam.content
        , pparam.uuid

    ];
    console.log('Sql_Params');
    console.log(Sql_Params);
    connection.query(sql, Sql_Params,
        function (error, rows, fields) {

            if (error) {

                console.log(error);

                result = {result:"0"}
                JSON.stringify(result)
                fn(result)
                return 0;

            }
            result = JSON.stringify(rows);
            console.log("updateorder返回值:........", result)
            result = {result:"1"}
            JSON.stringify(result)
            fn(result)
            return 1;
        });
    //connection.end();
}

function insertorder(param, fn) {
    // connection.connect();
    try {

        var deparam = decodeURIComponent(querystring.stringify(param));
        var pparam = querystring.parse(deparam);
        console.log('deparam');
        console.log(pparam);
        var AddSql = 'INSERT INTO trip (phonenumber ,gdate ,leaveplace,arriveplace,flightno,finished,uuid,tradeid,useflag,payflag,refundflag,evaflag,formprice,declprice,price,orderdate,status,evalevel,content ) \n' +
            'VALUE\n' +
            '\t( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,? )';

        var AddSql_Params = [
            pparam.phonenumber
            , pparam.gdate
            , pparam.leaveplace
            , pparam.arriveplace
            , pparam.flightno
            , pparam.finished
            , pparam.uuid
            , pparam.tradeid
            , pparam.useflag
            , pparam.payflag
            , pparam.refundflag
            , pparam.evaflag
            , pparam.formprice
            , pparam.declprice
            , pparam.price
            , pparam.orderdate
            , 1
            , pparam.evalevel
            , pparam.content
        ];
        console.log('执行sql:' + AddSql);
        //检查用户手机号是否注册过

        var sql = 'SELECT count(*) ct from trip where uuid=?';
        var sql_Params = [pparam.uuid];
        var queryresult = '';
        connection.query(sql, sql_Params,
            function (error, rows, fields) {

                if (error) {

                    console.log(error);

                    return;

                }

                if (rows[0].ct != 0) {
                    fn('0');
                    updateorder(pparam)

                } else {
                    connection.query(AddSql, AddSql_Params, function (err, results) {
                        iresult = '订单成功';
                        if (err) {
                            throw err;
                            iresult = '0';
                        }
                        else {
                            iresult = '1';
                        }
                        console.log(iresult);
                        fn(iresult);

                    });

                }
            });

//增 add

    }
    catch (e) {
        console.log(e.message);
    }
    //connection.end();

}
//
//function deleteorder(param, fn) {
//    // connection.connect();
//    try {
//        var iresult = '';
//        var deparam = decodeURIComponent(querystring.stringify(param));
//        var pparam = querystring.parse(deparam);
//        console.log('deparam');
//        console.log(pparam);
//        var AddSql = 'delete from trip where tradeid=?';
//        var AddSql_Params = [
//        ];
//
//        //检查用户手机号是否注册过
//        connection.query(AddSql, AddSql_Params, function(err, results) {
//            if (err) {
//                throw err;
//                iresult = '删除订单失败';
//            }
//            else {
//                iresult = '删除订单成功';
//            }
//            console.log(iresult);
//            fn('删除订单成功');
//
//        });
//
//    }
//    catch (e) {
//        console.log(e.message);
//    }
//
//}

module.exports = {
    insertorder: insertorder,
    updateorder: updateorder,
    order: order,
};

