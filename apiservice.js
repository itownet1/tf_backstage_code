/**
 * Created by Administrator on 2018/8/23.
 */
//设计第三方用户调用接口所有的方法
var mysql = require('mysql');
var http = require('http');
var https = require('https');
var querystring = require('querystring');
var url = require('url');
var async = require('async');
var share = require('./share.js')
var ocr = require('./ocr.js')
var pool = share.pool;
var qr_image = require('qr-image')
var fs = require('fs')
var AipOcrClient = require("baidu-aip-sdk").ocr;
function execTrans(sqlparamsEntities, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        }
        connection.beginTransaction(function (err) {
            if (err) {
                return callback(err, null);
            }
            console.log('开始执行transaction，共执行' + sqlparamsEntities.length + '条数据');
            var funcAry = [];
            sqlparamsEntities.forEach(function (sql_param) {
                var temp = function (cb) {
                    var sql = sql_param.sql;
                    var param = sql_param.params;

                    console.log('事务中的参数:---');
                    console.log(param);
                    connection.query(sql, param, function (tErr, rows, fields) {
                        if (tErr) {
                            connection.rollback(function () {
                                console.log('事务失败，' + sql_param + '，ERROR：' + tErr);
                                throw tErr;
                            });
                        } else {
                            if (sql.indexOf("select") > -1) {
                                return callback('', rows);
                            } else {
                                return cb(null, 'ok');
                            }


                        }
                    });
                };
                funcAry.push(temp);
            });

            async.series(funcAry, function (err, result) {
                console.log('transaction error: ' + err);
                if (err) {
                    connection.rollback(function (err) {
                        console.log('transaction error: ' + err);
                        connection.release();
                        return callback(err, null);
                    });
                } else {
                    connection.commit(function (err, info) {
                        console.log('transaction info: ' + JSON.stringify(info));
                        if (err) {
                            console.log('执行事务失败，' + err);
                            connection.rollback(function (err) {
                                console.log('transaction error: ' + err);
                                connection.release();
                                return callback(err, null);
                            });
                        } else {
                            connection.release();
                            return callback(null, info);
                        }
                    });
                }
            });
        });
    });
}

function _getNewSqlParamEntity(sql, params, callback) {
    if (callback) {
        return callback(null, {
            sql: sql,
            params: params,
        });
    }
    return {
        sql: sql,
        params: params,
    };
}


//获取n位随机数,随机来源chars
var chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
function generateMixed(n) {
    var res = "";
    for (var i = 0; i < n; i++) {
        var id = Math.ceil(Math.random() * 61);
        res += chars[id];
    }
    return res;
}


//用户注册写入mysql数据库

function apiuser(param, fn) {

    //appid获取
    var appid = new Date().getTime()
    console.log("appid:" + appid)
    var ak = generateMixed(24);
//获取32位sk
    var sk = generateMixed(32);

    var sqlParamsEntity = [];
    var ins_sql = 'insert into foreignservice values (?,?,?,?) ';
    var conditon_param = [
        appid, ak, sk, '入境通中国入境卡接口'
    ];
    var obj = {
        appid: appid,
        ak: ak,
        sk: sk
    }
    console.log(obj)

    sqlParamsEntity.push(_getNewSqlParamEntity(ins_sql, conditon_param));
    execTrans(sqlParamsEntity, function (err, info) {
        if (err) {
            console.error(err);
            fn('error')
        } else {
            console.log(obj);
            fn(obj)
        }
    });

}


/**
 * 护照图片识别
 */

function portocr(tempfilename, ocrkey, fn) {


//------------------百度识别

//// 设置APPID/AK/SK
//    var APP_ID = "11733602";
//    var API_KEY = "hYgkdeMrWrP5ctx5b08nXzEm";
//    var SECRET_KEY = "Gu5TpoTq2GULPA6fYGP3Q406CG9HQuKX";
//
//// 新建一个对象，建议只保存一个对象调用服务接口
//    var client = new AipOcrClient(APP_ID, API_KEY, SECRET_KEY);
//// 调用通用文字识别（含生僻字版）, 图片参数为本地图片
//    client.generalEnhance(image).then(function(result) {
//        console.log(JSON.stringify(result));
//    }).catch(function(err) {
//        // 如果发生网络错误
//        console.log(err);
//    });
    //https.get(
    //    {
    //        hostname: 'aip.baidubce.com',
    //        path: '/oauth/2.0/token?' + param,
    //        agent: false
    //    },
    //    function (res) {
    //        // 在标准输出中查看运行结果
    //
    //        var temp= res.pipe(process.stdout)
    //
    //        console.log("百度密钥：-------")
    //        console.log(temp)
    //    }
    //);
//------------------百度识别


    //var imageBuf = fs.readFileSync(url);
    //var image = imageBuf.toString("base64")
    console.log("ocr.sign", ocr.sign)
    var url = "https://mp.itownet.cn/linshi/" + tempfilename
    var param = {
        'appid': '1255823769',
        'url': url
    }


    var options = {
        host: 'recognition.image.myqcloud.com',
        path: '/ocr/handwriting',
        method: 'POST',
        headers: {
            "Host": 'recognition.image.myqcloud.com',
            'Content-Length': JSON.stringify(param).length,
            "Content-Type": 'application/json',
            "Authorization": ocr.sign
        }
    }

    var result = ''
    var req = https.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (data) {
            result += data
        });
        res.on('end', () => {
            console.log("data:", JSON.parse(result));

            try {
                var tempocr = []

                for (var i = 0; i < JSON.parse(result).data.items.length; i++) {
                    tempocr.push({
                        itemstring: JSON.parse(result).data.items[i].itemstring
                    })
                }

                //根据护照照片中最后两行内容识别
                var temp1 = tempocr[tempocr.length - 1].itemstring
                var temp2 = tempocr[tempocr.length - 2].itemstring
                var temp3 = temp2.substring(temp2.indexOf('<') + 2, temp2.length - temp2.indexOf('<') - 2)

                console.log("temp1" + temp1)
                console.log("temp2" + temp2)
                console.log("temp3" + temp3)
                var port_type = temp2.substring(0, 1)
                var port_countrycode = temp2.substring(2, 5)
                var port_firstname = temp2.substring(5, 9)

                var port_lastname = temp3.substring(0, temp3.indexOf('<'))

                var port_code = temp1.substring(0, 9)
                var port_birthdate = temp1.substring(13, 19)
                port_birthdate = port_birthdate.substring(0, 2) + '-' + port_birthdate.substring(2, 4) + '-' + port_birthdate.substring(4, 6)
                var port_sex = temp1.substring(20, 21) == 'F' ? '女' : '男'
                var port_limit = temp1.substring(21, 27)
                port_limit = port_limit.substring(0, 2) + '-' + port_limit.substring(2, 4) + '-' + port_limit.substring(4, 6)

                console.log(port_type)
                console.log(port_countrycode)
                console.log(port_firstname)
                console.log(port_lastname)
                console.log(port_code)
                console.log(port_birthdate)
                console.log(port_sex)
                console.log(port_limit)
                var list = {
                    porttype: port_type,
                    portcode: port_code,
                    countrycode: port_countrycode,
                    country: '',
                    firstname: port_firstname,
                    lastname: port_lastname,
                    birthday: port_birthdate,
                    birthplace: '',
                    issuancedate: '',
                    issuanceplace: '',
                    issuanceauthority: '',
                    sex: port_sex,
                    limitdate: port_limit
                }
                fn(list)


                console.log('响应中已无数据。');

            }
            catch (e) {
                /*异常无法被捕获,导致进程退出*/
                log.logger.info("图片识别异常" + e.message);
                return;
            }
        });
    });
    req.write(JSON.stringify(param))
    req.end

}


//第三方接口调用中国入境卡表单接口
function apichnentry(param, imgfile, fn) {
    //身份校验
    var sqlParamsEntity = [];
    var sel_sql = 'select count(*) as re from foreignservice where appid=? and apikey=? and secretkey=? ';
    var conditon_param = [
        param.appid, param.ak, param.sk
    ];
    var formlist = null

    sqlParamsEntity.push(_getNewSqlParamEntity(sel_sql, conditon_param));
    execTrans(sqlParamsEntity, function (err, info) {
        if (err) {

            console.error(err);
            fn('error')
        } else {
            console.log("查询结果");
            console.log(info[0].re);

            //校验成功info[0].re=1
            if (info[0].re > 0) {


                portocr(imgfile, ocr.sign, function (list) {
                    console.log(list)
                    formlist = list


                    var temp_qrcode = qr_image.image(JSON.stringify(formlist), {ec_level: 'H'})//设置容错率level为30%

                    temp_qrcode.pipe(require('fs').createWriteStream(__dirname + '/' + imgfile + '.png').on('finish', function () {
                        fn(__dirname + '/' + imgfile + '.png')  //验证成功
                        console.log('write finished')
                    }))


                })

                //识别上传的图片，返回入境卡json


                obj = {
                    birthday: "N8-30-42",
                    firstname: "LIU<",
                    flag: "JP",
                    flightno: "9999",
                    lastname: "XUAN",
                    otheroption1: "0",
                    otheroption2: "0",
                    phonenumber: "13654241826",
                    portcode: "EC541 769",
                    uuid: "201882214383417707"
                }


            } else {
                fn('error')
            }

        }
    });

}

//var ff = apichnentry({ appid: 1534992612943,
//    ak: 'JXd8iKS4EjToMEHCkLfHlDOL',
//    sk: 'hAfwC8MGaXqPSH8lprQWaTo1VGBgzOvL' },__dirname+'\qr.png', function (data) {
//    console.log("执行结果：");
//    console.log(data);
//
//
//})

//var ff1 = apiuser('', function (data) {
//    console.log("执行结果：");
//    console.log(data);
//})


module.exports = {
    apiuser: apiuser,
    apichnentry: apichnentry,
    portocr: portocr
};