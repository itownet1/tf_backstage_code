var mysql = require('mysql');
var httpserver = require('http');
var querystring = require('querystring');
var url = require('url');
var async = require('async');
var share = require('./share.js')
var connection = share.connection;
var pool = share.pool;
function execTrans(sqlparamsEntities, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        }
        connection.beginTransaction(function (err) {
            if (err) {
                return callback(err, null);
            }
            console.log('开始执行transaction，共执行' + sqlparamsEntities.length + '条数据');
            var funcAry = [];
            sqlparamsEntities.forEach(function (sql_param) {
                var temp = function (cb) {
                    var sql = sql_param.sql;
                    var param = sql_param.params;

                    console.log('事务中的参数:---');
                    console.log(param);
                    connection.query(sql, param, function (tErr, rows, fields) {
                        if (tErr) {
                            connection.rollback(function () {
                                console.log('事务失败，' + sql_param + '，ERROR：' + tErr);
                                throw tErr;
                            });
                        } else {
                            return cb(null, 'ok');
                        }
                    });
                };
                funcAry.push(temp);
            });

            async.series(funcAry, function (err, result) {
                console.log('transaction error: ' + err);
                if (err) {
                    connection.rollback(function (err) {
                        console.log('transaction error: ' + err);
                        connection.release();
                        return callback(err, null);
                    });
                } else {
                    connection.commit(function (err, info) {
                        console.log('transaction info: ' + JSON.stringify(info));
                        if (err) {
                            console.log('执行事务失败，' + err);
                            connection.rollback(function (err) {
                                console.log('transaction error: ' + err);
                                connection.release();
                                return callback(err, null);
                            });
                        } else {
                            connection.release();
                            return callback(null, info);
                        }
                    });
                }
            });
        });
    });
}

function _getNewSqlParamEntity(sql, params, callback) {
    if (callback) {
        return callback(null, {
            sql: sql,
            params: params,
        });
    }
    return {
        sql: sql,
        params: params,
    };
}


function syncform(pphonenumber, param, fn) {
    var sqlParamsEntity = [];

    var del_sql = 'delete from form where phonenumber=?';
    var del_param = {phonenumber: pphonenumber};
    var conditon_param = [del_param.phonenumber]
    sqlParamsEntity.push(_getNewSqlParamEntity(del_sql, conditon_param));

    var ins_sql = 'insert into form (phonenumber,form) values (?,?)';


    var ins_param = JSON.stringify(param);

    conditon_param = [
        pphonenumber
        , ins_param

    ];
    console.log('conditon_param');
    console.log(conditon_param);
    sqlParamsEntity.push(_getNewSqlParamEntity(ins_sql, conditon_param));

    console.log('sqlParamsEntity');
    console.log(sqlParamsEntity);
    execTrans(sqlParamsEntity, function (err, info) {
        if (err) {
            console.error(err);
            console.error('同步表单失败');
            fn('同步表单失败.')
        } else {
            console.log('同步表单成功.');
            fn('同步表单成功.')
        }
    });

}


function syncdecl(pphonenumber, param, fn) {
    var sqlParamsEntity = [];

    var del_sql = 'delete from decl where phonenumber=?';
    var del_param = {phonenumber: pphonenumber};
    var conditon_param = [del_param.phonenumber]
    sqlParamsEntity.push(_getNewSqlParamEntity(del_sql, conditon_param));

    var ins_sql = 'insert into decl (phonenumber,declform) values (?,?)';


    var ins_param = JSON.stringify(param);

    conditon_param = [
        pphonenumber
        , ins_param

    ];
    console.log('conditon_param');
    console.log(conditon_param);
    sqlParamsEntity.push(_getNewSqlParamEntity(ins_sql, conditon_param));

    console.log('sqlParamsEntity');
    console.log(sqlParamsEntity);
    execTrans(sqlParamsEntity, function (err, info) {
        if (err) {
            console.error(err);
            console.error('同步海关申报单失败');
            fn('同步海关申报单失败.')
        } else {
            console.log('同步海关申报单成功.');
            fn('同步海关申报单成功.')
        }
    });

}

//
//function syncform(pphonenumber, param, fn) {
//  var sqlParamsEntity = [];
//  var del_sql = 'delete from decl_form where phonenumber=?';
//  var del_param = {phonenumber: pphonenumber};
//  var conditon_param = [del_param.phonenumber]
//
//  //var deparam = decodeURIComponent(querystring.stringify(param));
//  console.log('conditon_param');
//  console.log(conditon_param);
//
//
//  sqlParamsEntity.push(_getNewSqlParamEntity(del_sql, conditon_param));
//
//  var ins_sql = 'insert into decl_form (firstname,lastname,birthday,flightno,is_select_fir,is_select_sec,phonenumber,tripid) values (?,?,?,?,?,?,?,?)';
//  var ins_param = null;
//  for (i = 0; i < param.length; i++) {
//    ins_param = param[i];
//    conditon_param = [
//      ins_param.firstname
//      , ins_param.lastname
//      , ins_param.birthday
//      , ins_param.flightno
//      , ins_param.otheroption1
//      , ins_param.otheroption2
//      , ins_param.phonenumber
//      , ins_param.uuid
//
//    ];
//    console.log('conditon_param');
//    console.log(conditon_param);
//    sqlParamsEntity.push(_getNewSqlParamEntity(ins_sql, conditon_param));
//  }
//  console.log('sqlParamsEntity');
//  console.log(sqlParamsEntity);
//  execTrans(sqlParamsEntity, function(err, info) {
//    if (err) {
//      console.error(err);
//      console.error('同步表单失败');
//      fn('同步表单失败.')
//    } else {
//      console.log('同步表单成功.');
//      fn('同步表单成功.')
//    }
//  });
//
//}



function form(param, fn) {
    var result = '';
    //connection.connect();
    var sql = 'SELECT *  from form where phonenumber=? ';

    var deparam = decodeURIComponent(querystring.stringify(param));
    var pparam = querystring.parse(deparam);

    var sql_Params = [
        pparam.phonenumber
    ];

    console.log('Sql_Params');
    console.log(sql_Params);


    connection.query(sql, sql_Params,
        function(error, rows, fields) {

            if (error) {

                console.log(error);

                return;

            }
            var tempobj = rows

            result = getformbyuuid( pparam.uuid,rows[0].form);

            fn(result);

        });
    return result;
    //connection.end();
}

function getformbyuuid(uuid,obj) {
    var result = []
    var jsonobj= {jsonobj:JSON.parse(obj)}
    console.log('getformbyuuid拆分结果：',jsonobj.jsonobj);
    for(var i=0;i<jsonobj.jsonobj.length;i++)
    {

        console.log('obj的结果：',jsonobj.jsonobj[i]);
        if (jsonobj.jsonobj[i].uuid==uuid){
            result.push(jsonobj.jsonobj[i])
        }
    }
    return result
}


module.exports = {
    syncform: syncform,
    syncdecl: syncdecl,
    form:form
};

