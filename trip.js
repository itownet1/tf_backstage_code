var mysql = require('mysql');
var httpserver = require('http');
var querystring = require('querystring');
var url = require('url');
var async = require('async');
var share = require('./share.js')
var connection = share.connection;
var pool = share.pool;
function execTrans(sqlparamsEntities, callback) {
  pool.getConnection(function(err, connection) {
    if (err) {
      return callback(err, null);
    }
    connection.beginTransaction(function(err) {
      if (err) {
        return callback(err, null);
      }
      console.log('开始执行transaction，共执行' + sqlparamsEntities.length + '条数据');
      var funcAry = [];
      sqlparamsEntities.forEach(function(sql_param) {
        var temp = function(cb) {
          var sql = sql_param.sql;
          var param = sql_param.params;

          console.log('事务中的参数:---');
          console.log(param);
          connection.query(sql, param, function(tErr, rows, fields) {
            if (tErr) {
              connection.rollback(function() {
                console.log('事务失败，' + sql_param + '，ERROR：' + tErr);
                throw tErr;
              });
            } else {
              return cb(null, 'ok');
            }
          });
        };
        funcAry.push(temp);
      });

      async.series(funcAry, function(err, result) {
        console.log('transaction error: ' + err);
        if (err) {
          connection.rollback(function(err) {
            console.log('transaction error: ' + err);
            connection.release();
            return callback(err, null);
          });
        } else {
          connection.commit(function(err, info) {
            console.log('transaction info: ' + JSON.stringify(info));
            if (err) {
              console.log('执行事务失败，' + err);
              connection.rollback(function(err) {
                console.log('transaction error: ' + err);
                connection.release();
                return callback(err, null);
              });
            } else {
              connection.release();
              return callback(null, info);
            }
          });
        }
      });
    });
  });
}

function _getNewSqlParamEntity(sql, params, callback) {
  if (callback) {
    return callback(null, {
      sql: sql,
      params: params,
    });
  }
  return {
    sql: sql,
    params: params,
  };
}

function synctrip(pphonenumber, param, fn) {
  var sqlParamsEntity = [];
  var del_sql = 'delete from trip where phonenumber=?';
  var del_param = {phonenumber: pphonenumber};
  var conditon_param = [del_param.phonenumber]

  //var deparam = decodeURIComponent(querystring.stringify(param));
  console.log('conditon_param');
  console.log(conditon_param);


  sqlParamsEntity.push(_getNewSqlParamEntity(del_sql, conditon_param));

  var ins_sql = 'insert into trip (phonenumber ,gdate ,leaveplace,arriveplace,flightno,uuid,finished) values (?,?,?,?,?,?,?)';
  var ins_param = null;
  for (i = 0; i < param.length; i++) {
    ins_param = param[i];
    conditon_param = [
      ins_param.phonenumber
      , ins_param.gdate
      , ins_param.leaveplace
      , ins_param.arriveplace
      , ins_param.flightno
        , ins_param.uuid
        ,ins_param.status
    ];
    console.log('conditon_param');
    console.log(conditon_param);
    sqlParamsEntity.push(_getNewSqlParamEntity(ins_sql, conditon_param));
  }
  console.log('sqlParamsEntity');
  console.log(sqlParamsEntity);
  execTrans(sqlParamsEntity, function(err, info) {
    if (err) {
      console.error(err);
      console.error('同步行程信息失败');
      fn('同步行程信息失败.')
    } else {
      console.log('同步行程信息成功.');
      fn('同步行程信息成功.')
    }
  });

}

module.exports = {
  synctrip: synctrip,
};

