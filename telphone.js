var WXBizDataCrypt = require('./WXBizDataCrypt');

function getnumber(pdata,piv,psessionkey) {
    var appId = 'wx1281bf8844793932';
    var sessionKey =psessionkey;

    var encryptedData = pdata;
    var iv = piv;


    var pc = new WXBizDataCrypt(appId, sessionKey);

    var data = pc.decryptData(encryptedData, iv);

    //console.log('解密后 data: ', data.phoneNumber);
    return data;

};
exports.getnumber = getnumber;