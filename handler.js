var fs = require('fs');
var post = require('./post.js');
var telphone = require('./telphone.js');
var userdao = require('./userdao');
var querystring = require('querystring');
var contact = require('./contact.js');
var httpserver = require("http");
var url = require("url");
var trip = require("./trip");
var person = require("./person");
var form= require("./form");
var express = require('express');
var ocrkey = require("./ocr.js")
const path = require('path');
//form表单需要的中间件。
var mutipart= require('connect-multiparty');
var mutipartMiddeware = mutipart();
var app = express();

function home(response) {
  response.writeHead(200, {'Content-Type': 'text/html'});
  fs.createReadStream(__dirname + '/index.html', 'utf8').pipe(response);
}

function review(response) {
  response.writeHead(200, {'Content-Type': 'application/json'});
  var jsonObj = {
    name: 'review',
  };
  response.end(JSON.stringify(jsonObj));
}

function api_records(response, param) {
  //发出请求，获得数据
  console.log('参数name：' + param.js_code);

  post.post('https://api.weixin.qq.com/sns/jscode2session', {
    appid: 'wx6fd4330cc9118ae5',
    secret: 'cb62e4f537017e6dc49dd61cd2db85d9',
    js_code: param.js_code,
    grant_type: 'authorization_code',
  }, function(data) {

    response.end(data);

    console.log(data);
  });

  response.writeHead(200, {'Content-Type': 'application/json'});
  var jsonObj = {
    name: 'account',
  };
}

function phone(response, param) {
  try {
  //发出请求，获得手机号码数据
  console.log('参数name：' + param.js_code);

  console.log('开始获取手机号');
  console.log(param.pdata);
  console.log(param.piv);
  console.log(param.psessionkey);
  var mn = telphone.getnumber(param.pdata, param.piv, param.psessionkey);

  // response.end(JSON.stringify(param));
  console.log('获取到手机号：' + JSON.stringify(mn));
  response.writeHead(200, {'Content-Type': 'application/json'});

  response.end(JSON.stringify(mn));
  }
  catch (e) {
    mn = {
      phonenumber:"error"
    }
    response.writeHead(200, {'Content-Type': 'application/json'});

    response.end(JSON.stringify(mn));

  }
}

function syncuser(response, param) {
  //发出请求，获得数据
  console.log('开始同步后台数据');
  var result = '';
  var deparam = decodeURIComponent(JSON.stringify(param));
  console.log('参数name：' + deparam);
 	//对表单数据进行解码
  try {
    result = userdao.insertuser(JSON.parse(deparam));
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    result = '用户信息入库失败'+e.message;
  }
  response.writeHead(200, { 'Content-Type' : 'text/html; charset=UTF-8' });
  response.end(result);
}



function synctrip(response, param) {
  console.log('synctrip的参数');
  console.log(param);
  var mn = param[0].phonenumber

  console.log('获取到手机号：' + JSON.stringify(mn));

  response.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
  try {
    var  result = trip.synctrip(mn,param,function(data){
      console.log("synctrip的结果");
      response.end(data);
      console.log(data);
    });
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    response.end('用户信息入库失败'+e.message);
    return;
  }

}



function syncperson(response, param) {
  console.log('syncperson的参数');
  console.log(param);
  var mn = param[0].phonenumber

  console.log('获取到手机号：' + JSON.stringify(mn));

  response.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
  try {
    var  result = person.syncperson(mn,param,function(data){
      response.end(data);
    });
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    response.end('用户信息入库失败'+e.message);
    return;
  }

}


function syncform(response, param) {
  console.log('syncform的参数');
  console.log(param);
  var mn = param[0].phonenumber

  console.log('获取到手机号：' + JSON.stringify(mn));

  response.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
  try {
    var  result = form.syncform(mn,param,function(data){
      response.end(data);
    });
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    response.end('表单入库失败'+e.message);
    return;
  }

}

function trip(response, param) {
  //发出请求，获得手机号码数据

  var mn = param.phonenumber

  console.log('获取到手机号：' + JSON.stringify(mn));




  response.writeHead(200, {'Content-Type': 'application/json'});

  var  result = userdao.trip(JSON.parse(mn),function(data){
    console.log("trip执行结果：");
    console.log(data);
    response.end(data);

    console.log(data);
  });


}



function portcontact(response, param) {

  var mn = param.phonenumber

  console.log('联系人获取到手机号：' + JSON.stringify(mn));




  response.writeHead(200, {'Content-Type': 'application/json'});
//JSON.parse(mn)
  var  result = contact.contact(mn,function(data){
    console.log("contact执行结果：");
    console.log(data);
    response.end(data);

    console.log(data);
  });


}


function synccontact(response, param) {
  //发出请求，获得数据

  var deparam = decodeURIComponent(JSON.stringify(param));
  console.log('参数name：' + deparam);

  //对表单数据进行解码
  try {
    contact.insertcontact(JSON.parse(deparam),function(result){

      response.writeHead(200, { 'Content-Type' : 'text/paint; charset=UTF-8' });
      response.end(result);
    })



  } catch (e) {
    /*异常无法被捕获,导致进程退出*/


    result = '用户信息入库失败'+e.message;

  }



}





function deletecontact(response, param) {
  //发出请求，获得数据

  var deparam = decodeURIComponent(JSON.stringify(param));
  console.log('参数name：' + deparam);
  try {
    contact. deletecontact(JSON.parse(deparam),function(result){

      response.writeHead(200, { 'Content-Type' : 'text/paint; charset=UTF-8' });
      response.end(result);
    })
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    result = '用户信息入库失败'+e.message;
  }
}
function  uploadfile() {


  var mutipartMiddeware = mutipart();
  var app = express();
  app.use(mutipart({uploadDir:'./linshi'}));
  app.set('port',process.env.PORT || 8090);
  app.listen(app.get('port'),function () {
    console.log("Express started on http://localhost:"+app.get('port')+'; press Ctrl-C to terminate.');
  });

  app.post('/uploadfile',mutipartMiddeware,function (req,res) {
    console.log(req.files);
    res.send('upload success!');
  });

}


function ocr(response, param) {
  var result =null
  var obj = null
  //对表单数据进行解码
  try {
    console.log("ocr获取key")
     result = ocrkey.sign;
    obj = {ocrkey: result}
    console.log(obj)
  } catch (e) {
    /*异常无法被捕获,导致进程退出*/
    result = '用户信息入库失败'+e.message;
  }
  response.writeHead(200, { 'Content-Type' : 'application/json' });


  response.end(JSON.stringify(obj));
}


module.exports = {
  home: home,
  review: review,
  api_records: api_records,
  phone: phone,
  syncuser: syncuser,
  trip:trip,
  synccontact:synccontact,
  portcontact:portcontact,
  deletecontact:deletecontact,
  synctrip : synctrip,
  syncperson:syncperson,
  syncform : syncform,
  uploadfile:uploadfile,
  ocr:ocr
};