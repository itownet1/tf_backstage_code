﻿var http = require('http');
var https = require('https');
var fs = require('fs');
var url = require('url');
var iconv = require('iconv-lite');
var querystring = require('querystring');
var telphone = require('./telphone.js');
var trip = require("./trip");
var person = require("./person");
var form = require("./form");
var pay = require("./pay");
var forum = require("./forum");
const axios = require('axios')
const md5 = require('blueimp-md5')
const xml2js = require('xml2js')
const xmlParser = new xml2js.Parser()

var order = require('./order');
var userdao = require('./userdao');
var path = require('path');
var contact = require("./contact.js")
var apiservice = require("./apiservice.js")
var post = require('./post.js');
var usermessage = require('./usermessage.js');
var options = {
    key: fs.readFileSync('./2_mp.itownet.cn.key'),
    cert: fs.readFileSync('./1_mp.itownet.cn_bundle.crt'),
};


//导入日志模块
var log = require('./log');


//express使用的是@4版本的。
var express = require('express');
var bodyParser = require("body-parser");
//form表单需要的中间件。
var mutipart = require('connect-multiparty');

var mutipartMiddeware = mutipart();
var app = express();
var jpn = require('./jpn');




app.use(mutipart({uploadDir: './linshi'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));





function startServer(route, handle) {

    // var onRequest = function(request, response) {
    //   var pathname = url.parse(request.url).pathname;
    //   console.log('request method:' + request.method.toUpperCase());
    //   if (request.method.toUpperCase() == 'GET') {
    //     var param = url.parse(request.url, true).query;
    //
    //     route(handle, pathname, response, param);
    //   } else {
    //     console.log('method post : ' + pathname);
    //     var data = '';
    //     var param = '';
    //
    //     request.on('err', function(err) {
    //       console.error(err);
    //     }).on('data', function(chunk) {
    //       console.log('begin recieve data');
    //       data += chunk;
    //     }).on('end', function() {
    //       //console.log('end recieve data:  ' + data);
    //       if (pathname == '/upload') {
    //
    //
    //
    //       } else {
    //         param = JSON.parse(data);
    //         route(handle, pathname, response, param);
    //       }
    //       //param = JSON.parse(data);
    //
    //       //图片作为参数，不能使用json来上传
    //
    //
    //     });
    //
    //   }
    // };
//-----------------------启动https服务，输出模块server----------------
    // -----------------------------------------------------------------//
    //var server = https.createServer(options, app).listen('443', '192.168.32.13', function () {
    //    console.log('Express started on https://192.168.32.13:443');
    //    log.logger.info("开启服务，服务端口为443");
    //});

var server = http.createServer(app).listen('9999','192.168.0.102',function(){
    console.log("应用实例，访问地址为 http://192.168.0.102:9999")
});


    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());


//获取img目录下的文件
    app.get('/img/:fileName', function (req, res) {
        log.logger.info("访问文件接口/linshi+filename...");
        var fileName = req.params.fileName;
        var filePath = path.join(__dirname + "\/img\/", fileName);
        res.sendFile(filePath);
        console.log("Request for " + filePath + " received.");

    })


    //跟目录下访问文件
    app.get('/:fileName', function (req, res) {
        log.logger.info("访问文件接口/linshi+filename...");
        var fileName = req.params.fileName;
        var filePath = path.join(__dirname + "\/", fileName);

        if (fileName == "notice.json") {
            //整理为json对象返回
            console.log(filePath)
            fs.readFile(filePath, 'utf8', function (err, data) {
                console.log("文件内容")
                console.log(data);
                var obj = JSON.parse(data)
                res.send(obj)
            });
            return
        }


        res.sendFile(filePath);
        console.log("Request for " + filePath + " received.");

    })


//上传文件
    app.post('/upload', mutipartMiddeware, function (req, res) {
        log.logger.info("访问上传文件接口/upload...");
        log.logger.info("源文件名：" + req.files.upfile.name);
        log.logger.info("临时文件名：" + req.files.upfile.path);
        console.log("---------------：");
        console.log(req.files)
        console.log("源文件名：" + req.files.upfile.name);
        console.log("临时文件名：" + req.files.upfile.path);
        var newname = __dirname + "\/" + req.files.upfile.path

        var oldname = __dirname + "\/linshi\/" + req.files.upfile.name
        fs.readFile(oldname, function (err, data) {

            fs.rename(newname, oldname)
            console.log("上传文件重命名完成");
            log.logger.info("上传文件重命名完成");

        })


        res.send('upload success!');
    });
//获取文件
    app.get('/linshi/:fileName', function (req, res) {
        log.logger.info("访问文件接口/linshi+filename...");
        var fileName = req.params.fileName;
        var filePath = path.join(__dirname + "\/linshi\/", fileName);

        if (fileName == "notice.json") {
            //整理为json对象返回
            console.log(filePath)
            fs.readFile(filePath, 'utf8', function (err, data) {
                console.log("文件内容")
                console.log(data);
                var obj = JSON.parse(data)
                res.send(obj)
            });
            return
        } else
            res.sendFile(filePath);
        console.log("Request for " + filePath + " received.");

    })


//证书认证专用
    app.get('/.well-known/pki-validation/:fileName', function (req, res) {
        log.logger.info("访问文件接口/.well-known/pki-validation/+" + req.params.fileName + "...");
        var fileName = req.params.fileName;
        var filePath = path.join(__dirname + "\/.well-known\/pki-validation\/", fileName);

        res.sendFile(filePath);
        console.log("Request for " + filePath + " received.");

    })


//获取用户session_key
    app.post('/account', function (req, res) {

        try {

            log.logger.info("访问用户帐号接口/account...");
            log.logger.info("报文内容");
            log.logger.info(req.body);
            console.log(req.body);

            // post.post('https://api.weixin.qq.com/sns/jscode2session', {
            //   appid: 'wx6fd4330cc9118ae5',
            //   secret: 'cb62e4f537017e6dc49dd61cd2db85d9',
            //   js_code: req.body.js_code,
            //   grant_type: 'authorization_code',
            // }, function(data) {
            //
            //   console.log(data);
            //   res.send(data);
            // });


            post.post('https://api.weixin.qq.com/sns/jscode2session', {
                appid: 'wx1281bf8844793932',
                secret: 'f5fdbea854f1edae2e7ca8b8e0318fd7',
                js_code: req.body.js_code,
                grant_type: 'authorization_code',
            }, function (data) {
                log.logger.info("微信端返回session验证数据");
                log.logger.info(data);
                console.log(data);
                res.send(data);
            });

        }
        catch (e) {
            res.send({msg: "error:api.weixin.qq.com"});
        }

    })

//获取access_token
    app.post('/account', function (req, res) {

        try {

            log.logger.info("访问用户帐号接口/account...");
            log.logger.info("报文内容");
            log.logger.info(req.body);
            console.log(req.body);


            post.post('https://api.weixin.qq.com/sns/jscode2session', {
                appid: 'wx1281bf8844793932',
                secret: 'f5fdbea854f1edae2e7ca8b8e0318fd7',
                js_code: req.body.js_code,
                grant_type: 'authorization_code',
            }, function (data) {
                log.logger.info("微信端返回session验证数据");
                log.logger.info(data);
                console.log(data);
                res.send(data);
            });

        }
        catch (e) {
            res.send({msg: "error:api.weixin.qq.com"});
        }

    })



//微信支付接口
    app.post('/paysign', function (req, res) {

        log.logger.info("访问数据接口/paysign...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('paysign的参数');
        console.log(req.body);
        const appId = 'wx1281bf8844793932'
// 商户号
        const mchId = '1534357841'
        // attach 是一个任意的字符串, 会原样返回, 可以用作一个标记
        const attach = 'itownet_rjt'
        const productIntro = '入境通订单支付'
// 一个随机字符串
        const nonceStr = pay.getNonceStr()
        //支付结果回调地址
        const notifyUrl = 'https://mp.itownet.cn/linshi/paysucc.html'
// 用户的 openId
        var openId = req.body.openid
        //订单总金额
        var price = req.body.price


        //调用api接口的ip
// 这里是在 express 获取用户的 ip, 因为使用了 nginx 的反向代理, 所以这样获取
        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        ip = ip.match(/\d+\.\d+\.\d+\.\d+/)[0]


// 生成商家内部自定义的订单号, 商家内部的系统用的, 理论上只要不和其他订单重复, 使用任意的字符串都是可以的
        const tradeId = pay.getTradeId(attach)
// 生成签名
        const sign = pay.getPrePaySign(appId, attach, productIntro, mchId, nonceStr, notifyUrl, openId, tradeId, ip, price)

        //将微信需要的数据拼成 xml 发送出去
        const sendData = pay.wxSendData(appId, attach, productIntro, mchId, nonceStr, notifyUrl, openId, tradeId, ip, price, sign)

        console.log("准备开始调用微信支付接口，生成支付参数json")
        console.log("senddata", sendData)
        console.log("sign", sign)
        console.log("tradeid", tradeId)
        console.log("ip", ip)
        console.log("nonceStr", nonceStr)
        console.log("openId", openId)
        console.log("price", price)


// 使用 axios 发送数据带微信支付服务器, 没错, 后端也可以使用 axios


        axios.post('https://api.mch.weixin.qq.com/pay/unifiedorder', sendData)
            .then(function (response) {
                log.logger.info(response.data);
                xmlParser.parseString(response.data, (err, success) => {
                    if (err) {
                        log.logger.info("paysign解析xml错误");
                    } else {
                        if (success.xml.return_code[0] === 'SUCCESS') {
                            const prepayId = success.xml.prepay_id[0]
                            const payParamsObj = pay.getPayParams(prepayId, tradeId)
                            console.log(payParamsObj);

                            log.logger.info("paysign解析完成" + payParamsObj);
                            // 返回给前端, 这里是 express 的写法
                            res.json(payParamsObj)
                        } else {
                            // 错误处理
                            if (err) {
                                log.logger.info("axios错误");
                                res.sendStatus(502)
                            } else if (success.xml.return_code[0] !== 'SUCCESS') {
                                res.sendStatus(403)
                            }
                        }
                    }
                })


            })
            .catch(function (error) {
                console.log(error);
            });


        //axios.post('https://api.mch.weixin.qq.com/pay/unifiedorder', sendData).then(wxResponse => {
        //    // 微信返回的数据也是 xml, 使用 xmlParser 将它转换成 js 的对象
        //    xmlParser.parseString(wxResponse.data, (err, success) => {
        //        if (err) {
        //            log.logger.info("paysign解析xml错误");
        //        } else {
        //            if (success.xml.return_code[0] === 'SUCCESS') {
        //                const prepayId = success.xml.prepay_id[0]
        //                const payParamsObj = pay.getPayParams(prepayId, tradeId)
        //                // 返回给前端, 这里是 express 的写法
        //                res.json(payParamsObj)
        //            } else {
        //                // 错误处理
        //                if (err) {
        //                    log.logger.info("axios错误");
        //                    res.sendStatus(502)
        //                } else if (success.xml.return_code[0] !== 'SUCCESS') {
        //                    res.sendStatus(403)
        //                }
        //            }
        //        }
        //    })
        //}).catch(err => {
        //    log.logger.info("paysign错误");
        //})

    })




//微信退款接口
    app.post('/refundsign', function (req, res) {

        log.logger.info("访问数据接口/refundsign...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        log.logger.info('refundsign的参数');
        log.logger.info(req.body);
        const appId = 'wx1281bf8844793932'
// 商户号
        const mchId = '1534357841'
        // attach 是一个任意的字符串, 会原样返回, 可以用作一个标记
        const attach = 'itownet_rjt'
        const productIntro = '入境通订单退款'
// 一个随机字符串
        const nonceStr = pay.getNonceStr()
        //支付结果回调地址
        const notifyUrl = 'https://mp.itownet.cn/linshi/paysucc.html'
// 用户的 openId
        var openId = req.body.openid
        //订单总金额
        var price = req.body.price
//退款订单号
        var tradeId =  req.body.tradeId

        //调用api接口的ip
// 这里是在 express 获取用户的 ip, 因为使用了 nginx 的反向代理, 所以这样获取
        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        ip = ip.match(/\d+\.\d+\.\d+\.\d+/)[0]


// 生成签名
        const sign = pay.getPrePaySign(appId, attach, productIntro, mchId, nonceStr, notifyUrl, openId, tradeId, ip, price)

        //将微信需要的数据拼成 xml 发送出去
        const sendData = pay.wxrefundData(appId, attach, productIntro, mchId, nonceStr, notifyUrl, openId, tradeId, ip, price, sign)

        log.logger.info("准备开始调用微信支付接口，生成支付参数json")
        log.logger.info("senddata", sendData)
        log.logger.info("sign", sign)
        log.logger.info("tradeid", tradeId)
        log.logger.info("ip", ip)
        log.logger.info("nonceStr", nonceStr)
        log.logger.info("openId", openId)
        log.logger.info("price", price)


// 使用 axios 发送数据带微信支付服务器, 没错, 后端也可以使用 axios


        axios.post('https://api.mch.weixin.qq.com/secapi/pay/refund', sendData)
            .then(function (response) {
                log.logger.info(response.data);
                xmlParser.parseString(response.data, (err, success) => {
                    if (err) {
                        log.logger.info("refundsign解析xml错误");
                    } else {
                        if (success.xml.return_code[0] === 'SUCCESS') {
                            const prepayId = success.xml.prepay_id[0]
                            const payParamsObj = pay.getPayParams(prepayId, tradeId)
                            log.logger.info(payParamsObj);

                            log.logger.info("refundsign解析完成" + payParamsObj);
                            // 返回给前端, 这里是 express 的写法
                            res.json(payParamsObj)
                        } else {
                            // 错误处理
                            if (err) {
                                log.logger.info("axios错误");
                                res.sendStatus(502)
                            } else if (success.xml.return_code[0] !== 'SUCCESS') {
                                res.sendStatus(403)
                            }
                        }
                    }
                })


            })
            .catch(function (error) {
                log.logger.info(error);
                res.sendStatus(900);
            });
    })

//获取用户手机号
    app.post('/phone', function (req, res) {
        log.logger.info("访问手机号接口/phone...");
        //发出请求，获得手机号码数据
        console.log('开始获取手机号');
        console.log(req.body.pdata);
        console.log(req.body.piv);
        console.log(req.body.psessionkey);
        var mn = telphone.getnumber(req.body.pdata, req.body.piv, req.body.psessionkey);
        console.log('获取到手机号：' + JSON.stringify(mn));
        log.logger.info('获取到手机号：' + JSON.stringify(mn));
        res.send(JSON.stringify(mn));
    })


    //注册用户
    app.post('/syncuser', function (req, res) {
        log.logger.info("访问同步用户数据接口/syncuser...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('开始同步后台数据');
        console.log(req.body)
        var result = '';
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        try {
            result = userdao.insertuser(JSON.parse(deparam));
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            result = '用户信息入库失败' + e.message;
        }
        res.send(result)

    })
    //同步行程数据
    app.post('/synctrip', function (req, res) {
        log.logger.info("访问同步行程数据接口/synctrip...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('synctrip的参数');
        console.log(req.body);
        var mn = req.body[0].phonenumber
        console.log('获取到手机号：' + JSON.stringify(mn));
        try {
            var result = trip.synctrip(mn, req.body, function (data) {
                console.log("synctrip的结果");
                res.send(data)
                console.log(data);
                log.logger.info("synctrip的结果");
                log.logger.info(data);
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }

    })

    //同步申请人数据
    app.post('/syncperson', function (req, res) {
        log.logger.info("访问同步申请人数据接口/syncperson...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('syncperson的参数');
        console.log(req.body);
        var mn = req.body[0].phonenumber

        console.log('获取到手机号：' + JSON.stringify(mn));
        try {
            var result = person.syncperson(mn, req.body, function (data) {
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }
    })

    //同步表单数据
    app.post('/syncform', function (req, res) {
        log.logger.info("访问同步表单数据接口/syncform...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('syncform的参数');
        console.log(req.body);
        var mn = req.body[0].phonenumber

        console.log('获取到手机号：' + JSON.stringify(mn));
        try {
            var result = form.syncform(mn, req.body, function (data) {
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }
    })


    //同步海关申报单数据
    app.post('/syncdecl', function (req, res) {
        log.logger.info("访问同步表单数据接口/syncdecl...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('syncdecl的参数');
        console.log(req.body);
        var mn = req.body[0].phonenumber

        console.log('获取到手机号：' + JSON.stringify(mn));
        try {
            var result = form.syncdecl(mn, req.body, function (data) {
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }
    })


    //显示入境卡数据

    app.get('/form', function (req, res) {
        console.log(req.query.phonenumber);
        console.log(req.query.uuid);

        var mn = {
            phonenumber: req.query.phonenumber,
            uuid: req.query.uuid
        }
        try {
            var result = form.form(mn, function (data) {
                console.log("form执行结果：");
                console.log(data);
                log.logger.info("form执行结果：");
                log.logger.info(data);
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }

    })


    //显示行程数据
    app.post('/trip', function (req, res) {
        log.logger.info("访问显示行程数据接口/trip...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var mn = req.body.phonenumber
        console.log('获取到手机号：' + JSON.stringify(mn));
        try {
            var result = userdao.trip(JSON.parse(mn), function (data) {
                console.log("trip执行结果：");
                console.log(data);
                log.logger.info("trip执行结果：");
                log.logger.info(data);
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }


    })

    //显示申请人数据
    app.post('/portcontact', function (req, res) {
        log.logger.info("访问显示申请人数据接口/portcontact...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var mn = req.body.phonenumber
        console.log('联系人获取到手机号：' + JSON.stringify(mn));
        try {
            var result = contact.contact(mn, function (data) {
                console.log("contact执行结果：");
                console.log(data);
                log.logger.info("contact执行结果：");
                log.logger.info(data);
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }


    })

    //同步申请人数据
    app.post('/synccontact', function (req, res) {
        log.logger.info("访问同步申请人数据接口/synccontact...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);

        //对表单数据进行解码
        try {
            contact.insertcontact(JSON.parse(deparam), function (result) {
                res.send(result)
            })
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;

        }
    })


    //删除申请人
    app.post('/deletecontact', function (req, res) {
        log.logger.info("访问删除申请人数据接口/deletecontact...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        try {
            contact.deletecontact(JSON.parse(deparam), function (result) {
                res.send(result)
            })
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }

    })

    //图片识别
    app.post('/ocr', function (req, res) {

        var result = null
        var obj = null
        //对表单数据进行解码
        try {
            console.log("ocr获取key")
            var ocrkey = require("./ocr.js")
            result = ocrkey.sign();
            obj = {ocrkey: result}
            console.log(obj)
            log.logger.info("访问上传文件接口/ocr...");
            log.logger.info(obj);
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            result = '用户信息入库失败' + e.message;
        }
        res.send(JSON.stringify(obj));

    })

    //航班动态接口，按起降城市
    app.post('/flight_city', function (req, res) {
        log.logger.info("访问航班动态接口/flight_city...");
        log.logger.info("报文内容");
        log.logger.info(req.body);

        var body = '';
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        //接受参数，调用远程接口，返回json
        var p_leavecity = req.body.leavecity
        var p_arrivecity = req.body.arrivecity
        var p_godate = req.body.godate
        var url = 'http://apis.haoservice.com/plan/InternationalFlightQueryByCity?dep=' + p_leavecity + '&arr=' + p_arrivecity + '&date=' + p_godate + '&key=80500be3c841431f9a80d787e6583d1f'


        http.get(url, function (gres) {
            console.log("Got response: " + gres);
            gres.on('data', function (d) {
                body += d;
            }).on('end', function () {
                //console.log(gres.headers);
                console.log(JSON.parse(body));

                log.logger.info("执行结果：");
                log.logger.info(JSON.parse(body));

                res.send(body);
            });
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        });


    })


    //航班动态接口，按航班号
    app.post('/flight_no', function (req, res) {
        log.logger.info("访问航班动态接口/flight_no...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var body = '';
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        //接受参数，调用远程接口，返回json
        var p_flightno = req.body.flightno
        var p_godate = req.body.godate
        var url = ' http://apis.haoservice.com/plan/FlightQueryByFlightNoV2?flightNo=' + p_flightno + '&date=' + p_godate + '&key=80500be3c841431f9a80d787e6583d1f'
        http.get(url, function (gres) {
            console.log("Got response: " + gres);
            gres.on('data', function (d) {
                body += d;
            }).on('end', function () {
                //console.log(gres.headers);
                console.log(body);
                log.logger.info("执行结果：");
                log.logger.info(body);
                res.send(body);
            });
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        });


    })
//第三方接口注册
    app.post('/apiuser', function (req, res) {
        log.logger.info("访问数据接口/apiuser...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('apiuser的参数');
        console.log(req.body);
        try {
            var result = apiservice.apiuser(req.body, function (data) {
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }
    })
    //第三方接口调用中国入境卡接口
    app.post('/apichnentry', function (req, res) {
        log.logger.info("访问数据接口/apichnentry...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var tfilename = req.body.filename
        var dataBuffer = new Buffer(req.body.filebuffer, 'base64');
        fs.writeFile('./linshi/' + tfilename, dataBuffer, function (err) {
            if (err) {
                console.log('文件保存失败')
            } else {
                console.log('文件保存成功')
                try {

                    var result = apiservice.apichnentry(req.body, tfilename, function (data) {
                        console.log('返回的图片路径', data)
                        var imageBuf = fs.readFileSync(data.toString()).toString('base64')
                        console.log(imageBuf)
                        res.send(imageBuf);
                    });
                } catch (e) {
                    /*异常无法被捕获,导致进程退出*/
                    res.send('用户信息入库失败' + e.message);
                    return;
                }

            }
        });


    })


    //用户消息下发
    app.post('/usermessage', function (req, res) {
        log.logger.info("访问用户消息数据接口/usermessage...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        console.log('传入参数' + req.body);

        try {
            //从后台数据库表获取未阅读的消息，生成json数据进行下发
            var result = usermessage.usermessage(req.body, function (data) {
                console.log("usermessage执行结果：");
                console.log(data);
                log.logger.info("usermessage执行结果：");
                log.logger.info(data);
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }


    })


    //获取订单信息
    app.post('/order', function (req, res) {
        log.logger.info("获取订单数据接口/deletecontact...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        try {
            order.order(JSON.parse(deparam), function (result) {
                res.send(result)
            })
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('订单获取失败' + e.message);
            return;
        }

    })


    //同步订单数据
    app.post('/insertorder', function (req, res) {
        log.logger.info("访问同步订单数据接口/insertorder...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);

        //对表单数据进行解码
        try {
            order.insertorder(JSON.parse(deparam), function (result) {
                res.send(result)
            })
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('订单入库失败' + e.message);
            return;

        }
    })


    //删除订单
    app.post('/updateorder', function (req, res) {
        log.logger.info("访问更新订单状态数据接口/deletecontact...");
        log.logger.info("报文内容");
        log.logger.info(req.body);
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        try {
            order.updateorder(JSON.parse(deparam), function (result) {
                res.send(result)
            })
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('更新订单失败' + e.message);
            return;
        }

    })


    //护照识别（腾讯提供接口）
    app.post('/passportocr',function(req,res){
        var deparam = decodeURIComponent(JSON.stringify(req.body));
        console.log('参数name：' + deparam);
        var imageurl = deparam;


            const tencentcloud = require("tencentcloud-sdk-nodejs");


            const OcrClient = tencentcloud.ocr.v20181119.Client;
            const models = tencentcloud.ocr.v20181119.Models;

            const Credential = tencentcloud.common.Credential;
            const ClientProfile = tencentcloud.common.ClientProfile;
            const HttpProfile = tencentcloud.common.HttpProfile;

            let cred = new Credential("AKIDUpw43pchngLm24BOgidKn7C3h9OPooIY", "B8uUv89ze5MPFrIUUFk00lGWaR2NUO3U");
            let httpProfile = new HttpProfile();
            httpProfile.endpoint = "ocr.tencentcloudapi.com";
            let clientProfile = new ClientProfile();
            clientProfile.httpProfile = httpProfile;
            let client = new OcrClient(cred, "ap-beijing", clientProfile);

            let reqnew = new models.PassportOCRRequest();

        //    let params = '{"ImageUrl":"https://mp.itownet.cn/linshi/port.jpg"}'

        let params = deparam;
        reqnew.from_json_string(params);


            client.PassportOCR(reqnew, function(errMsg, response) {

                if (errMsg) {
                    console.log(errMsg);
                    return;
                }

                //console.log(response.to_json_string());
                res.send(response.to_json_string())
            });


    })

//token验证

    app.get('/token', function (req, res) {
        log.logger.info("token验证");

        console.log(req.query)

        const signature = req.query.signature;
        const echostr = req.query.echostr;
        const timestamp = req.query.timestamp;
        const nonce = req.query.nonce;

        res.send(echostr)
    })

//显示论坛用户名
    app.get('/forum', function (req, res) {
        console.log(req.query.subject);


        var mn = {
            subject: req.query.subject,
             }
        try {
            var result = forum.user(mn, function (data) {
                console.log("forum执行结果：");
                console.log(data);
                log.logger.info("forum执行结果：");
                log.logger.info(data);
                res.send(data)
            });
        } catch (e) {
            /*异常无法被捕获,导致进程退出*/
            res.send('用户信息入库失败' + e.message);
            return;
        }

    })

}





module.exports.startServer = startServer;