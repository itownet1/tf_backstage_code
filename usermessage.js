/**
 * Created by Administrator on 2019/2/22.
 */
var mysql = require('mysql');
var httpserver = require('http');
var querystring = require('querystring');
var url = require('url');
var share = require('./share.js')
var connection = share.connection;
var pool = share.pool;

function usermessage(param, fn) {
    var result = '';
    var sql = 'SELECT *  from message where userid=? ';
    var sql_Params = [param.USERID];
    console.log(param.USERID);
    connection.query(sql, sql_Params,
        function(error, rows, fields) {

            if (error) {

                console.log(error);

                return;

            }
            result = JSON.stringify(rows);

            console.log('联系人查询结果：');
            console.log(result);
            fn(result);

        });
    return result;
    //connection.end();
}

function updateusermessage(param) {
    var result = '';
    //connection.connect();
    var sql = 'update passport ' +
        'set porttype              =?  \n' +
        ',portcode             =?\n' +
        ',countrycode          =?\n' +
        ',country              =?\n' +
        ',fullname             =?\n' +
        ',sex                  =?\n' +
        ',birthday             =?\n' +
        ',birthplace           =?\n' +
        ',issuancedate         =?\n' +
        ',issuanceplace        =?\n' +
        ',issuanceauthority    =?\n' +
        ',limitdate            =?\n' +
        ',phoneno          =?\n' +
        ',firstname           =?\n' +
        ',lastname            =?' +
        ' where phoneno=? and portcode=?';
    var deparam = decodeURIComponent(querystring.stringify(param));
    var pparam = querystring.parse(deparam);
    console.log('执行sql:'+sql);
    console.log(pparam);

    var Sql_Params = [
        pparam.porttype
        , pparam.portcode
        , pparam.countrycode
        , pparam.country
        , pparam.fullname
        , pparam.sex
        , pparam.birthday
        , pparam.birthplace
        , pparam.issuancedate
        , pparam.issuanceplace
        , pparam.issuanceauthority
        , pparam.limitdate
        , pparam.phonenumber
        , pparam.firstname
        , pparam.lastname
        , pparam.phonenumber
        , pparam.portcode,
    ];
    console.log('Sql_Params');
    console.log(Sql_Params);
    connection.query(sql, Sql_Params,
        function(error, rows, fields) {

            if (error) {

                console.log(error);

                return;

            }
            result = JSON.stringify(rows);
            return result;
        });
    //connection.end();
}

function insertusermessage(param, fn) {
    // connection.connect();
    try {

        var deparam = decodeURIComponent(querystring.stringify(param));
        var pparam = querystring.parse(deparam);
        console.log('deparam');
        console.log(pparam);
        var AddSql = 'INSERT INTO passport ( porttype, portcode, countrycode, country, fullname, sex, birthday, birthplace, issuancedate, issuanceplace, issuanceauthority, limitdate, phoneno,firstname,lastname ) \n' +
            'VALUE\n' +
            '\t( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,? )';

        var AddSql_Params = [
            pparam.porttype
            , pparam.portcode
            , pparam.countrycode
            , pparam.country
            , pparam.fullname
            , pparam.sex
            , pparam.birthday
            , pparam.birthplace
            , pparam.issuancedate
            , pparam.issuanceplace
            , pparam.issuanceauthority
            , pparam.limitdate
            , pparam.phonenumber
            , pparam.firstname
            , pparam.lastname,
        ];
        console.log('执行sql:'+AddSql);
        //检查用户手机号是否注册过

        var sql = 'SELECT count(*) ct from passport where phoneno=? and  portcode=?';
        var sql_Params = [pparam.phonenumber, pparam.portcode];
        var queryresult = '';
        connection.query(sql, sql_Params,
            function(error, rows, fields) {

                if (error) {

                    console.log(error);

                    return;

                }

                if (rows[0].ct != 0) {
                    fn('保存完成');
                    updatecontact(pparam)

                } else {
                    connection.query(AddSql, AddSql_Params, function(err, results) {
                        iresult = '添加申请人成功';
                        if (err) {
                            throw err;
                            iresult = '添加申请人失败';
                        }
                        else {
                            iresult = '添加申请人成功';
                        }
                        console.log(iresult);
                        fn('添加申请人成功');

                    });

                }
            });

//增 add

    }
    catch (e) {
        console.log(e.message);
    }
    //connection.end();

}

function deleteusermessage(param, fn) {
    // connection.connect();
    try {
        var iresult = '';
        var deparam = decodeURIComponent(querystring.stringify(param));
        var pparam = querystring.parse(deparam);
        console.log('deparam');
        console.log(pparam);
        var AddSql = 'delete from passport where portcode=? and phoneno=?';
        var AddSql_Params = [
            pparam.portcode
            , pparam.phonenumber,
        ];

        //检查用户手机号是否注册过
        connection.query(AddSql, AddSql_Params, function(err, results) {
            if (err) {
                throw err;
                iresult = '删除申请人失败';
            }
            else {
                iresult = '删除申请人成功';
            }
            console.log(iresult);
            fn('删除申请人成功');

        });

    }
    catch (e) {
        console.log(e.message);
    }

}

module.exports = {
    insertusermessage: insertusermessage,
    deleteusermessage: deleteusermessage,
    usermessage: usermessage,
};

